package com.setbeacon.vendor.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.setbeacon.vendor.R;
import com.setbeacon.vendor.activities.AddNewMemberActivity;
import com.setbeacon.vendor.activities.LoginActivity;
import com.setbeacon.vendor.adapters.UserListAdapter;
import com.setbeacon.vendor.controllor.ResponsehandlerListener;
import com.setbeacon.vendor.controllor.WebServiceClient;
import com.setbeacon.vendor.customviews.CustomTextViewBold;
import com.setbeacon.vendor.customviews.SimpleItemDecoration;
import com.setbeacon.vendor.models.LoginResponse;
import com.setbeacon.vendor.models.UserListResponse;
import com.setbeacon.vendor.utils.PreferenceHandler;
import com.setbeacon.vendor.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * `* SetBeacon_Vendor
 * Created by gsingh on 8/29/2016.
 */
public class ActiveUsersListFragment extends Fragment {
    private RecyclerView recyclerViewUsers;
    private CustomTextViewBold textViewNoMembers;
    private List<UserListResponse.Results> userList = new ArrayList<>();
    private UserListAdapter userListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_member_list, container, false);
        setupUI(v);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * setup UI components
     *
     * @param v
     */
    private void setupUI(View v) {
        recyclerViewUsers = (RecyclerView) v.findViewById(R.id.list_view_members);
        textViewNoMembers = (CustomTextViewBold) v.findViewById(R.id.tv_no_members);
        recyclerViewUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
        userListAdapter = new UserListAdapter(getActivity(), userList);
        //userListAdapter.setHasStableIds(true);
        recyclerViewUsers.setAdapter(userListAdapter);
        recyclerViewUsers.addItemDecoration(new SimpleItemDecoration(getActivity()));

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fetchListOfUsers(0);
    }

    /**
     * fetch list of users from server
     */
    private void fetchListOfUsers(final int pageNum) {
        WebServiceClient.getAllUsers(getActivity(), false, false, "1", pageNum, UserListResponse.class, new ResponsehandlerListener() {
            @Override
            public void onComplete(Object result, WebServiceClient.WebError error) {
                if (error == null) {
                    UserListResponse response = (UserListResponse) result;
                    if (response.getResults()!=null && response.getResults().size() > 0) {
                        recyclerViewUsers.setVisibility(View.VISIBLE);
                        textViewNoMembers.setVisibility(View.GONE);
                        userList = response.getResults();
                        userListAdapter.changeUserList(userList);
                        userListAdapter.notifyDataSetChanged();
                    } else {
                        if (userListAdapter.getItemCount() == 0) {
                            recyclerViewUsers.setVisibility(View.GONE);
                            textViewNoMembers.setVisibility(View.VISIBLE);
                        }
                    }

                } else {
                    switch (error) {
                        case UNAUTHORIZED:
                            WebServiceClient.getTokenRefreshed(getActivity(), false, LoginResponse.class, new ResponsehandlerListener() {
                                @Override
                                public void onComplete(Object result, WebServiceClient.WebError error) {
                                    if (error == null) {
                                        LoginResponse response = (LoginResponse) result;
                                        PreferenceHandler.writeString(getActivity(), PreferenceHandler.BEARER_TOKEN, response.getAccess_token());
                                        PreferenceHandler.writeString(getActivity(), PreferenceHandler.REFRESH_TOKEN, response.getRefresh_token());
                                        fetchListOfUsers(pageNum);
                                    } else {
                                        Utilities.clearPreferences(getActivity());
                                        Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(loginIntent);
                                        getActivity().finish();
                                    }
                                }
                            });
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(updateReciever, new IntentFilter("com.setbeacon.update"));
    }

    private BroadcastReceiver updateReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getUserVisibleHint())
                fetchListOfUsers(0);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_location, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    WebServiceClient.searchUser(getActivity(), query, "1", false, "0", UserListResponse.class, new ResponsehandlerListener() {
                        @Override
                        public void onComplete(Object result, WebServiceClient.WebError error) {
                            UserListResponse response = (UserListResponse) result;
                            if (response.getResults().size() > 0) {
                                recyclerViewUsers.setVisibility(View.VISIBLE);
                                textViewNoMembers.setVisibility(View.GONE);
                                //usersList = response.getResults();
                                userListAdapter.changeUserList(response.getResults());
                                userListAdapter.notifyDataSetChanged();
                            } else {
                                recyclerViewUsers.setVisibility(View.GONE);
                                textViewNoMembers.setVisibility(View.VISIBLE);
                                textViewNoMembers.setText(getResources().getString(R.string.no_inactive_members));
                            }

                        }
                    });
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });

            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    if (userList!=null && userList.size()>0){
                        recyclerViewUsers.setVisibility(View.VISIBLE);
                        textViewNoMembers.setVisibility(View.GONE);
                        userListAdapter.changeUserList(userList);
                        userListAdapter.notifyDataSetChanged();
                    }else{
                        recyclerViewUsers.setVisibility(View.GONE);
                        textViewNoMembers.setVisibility(View.VISIBLE);
                    }
                    return false;
                }
            });
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add_member:
                Intent addMemberIntent = new Intent(getActivity(),AddNewMemberActivity.class);
                startActivity(addMemberIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(updateReciever);
    }
}